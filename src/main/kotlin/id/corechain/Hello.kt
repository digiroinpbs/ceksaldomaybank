package id.corechain

import com.rethinkdb.RethinkDB
import com.rethinkdb.gen.exc.ReqlDriverError
import com.rethinkdb.gen.exc.ReqlError
import com.rethinkdb.gen.exc.ReqlPermissionError
import com.rethinkdb.net.Connection
import org.apache.commons.codec.digest.DigestUtils
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.chrome.ChromeDriver
import java.util.*
import redis.clients.jedis.Jedis
import redis.clients.jedis.exceptions.JedisConnectionException
import java.io.FileInputStream

class WebDriver(){
    fun getWebDriver(): WebDriver? {
        val props = Properties()
        val input = FileInputStream("/etc/service.conf");
        props.load(input)
        var driver: WebDriver
        System.setProperty("webdriver.chrome.driver", props.get("webdriver.chrome.driver").toString())
        driver = ChromeDriver()
        return driver
    }
    fun getDBName(): String? {
        val props = Properties()
        val input = FileInputStream("/etc/service.conf");
        props.load(input)
        return props.get("db.name").toString()
    }
    fun getRedisParam(): HashMap<String,String>? {
        val map:HashMap<String,String> = HashMap<String,String>()
        val props = Properties()
        val input = FileInputStream("/etc/service.conf");
        props.load(input)
        map.put("ip",props.get("redis.name").toString())
        map.put("port",props.get("redis.port").toString())
        map.put("password",props.get("redis.password").toString())
        map.put("db",props.get("redis.db").toString())
        return map
    }
}

fun main(args: Array<String>) {
    val username:String = "rizkyanies"
    val password:String = "123Qwe"
    try {
        checkSaldo(username, password)
    }catch (e:ReqlDriverError){
        System.out.println("connection rethink DB error")
    }catch (e:JedisConnectionException){
        System.out.println("connection redis error")
    }catch (e: ReqlPermissionError){
        System.out.println("connection rethink DB permission Error")
    }catch (e: ReqlError){
        System.out.println("rethink DB problem")
    }catch (e:Exception){
        e.printStackTrace()
    }
}

fun checkSaldo(username:String, password:String):String{
    var redisParam:HashMap<String,String> = id.corechain.WebDriver().getRedisParam() as HashMap<String, String>
    val jedis = Jedis(redisParam.get("ip"), redisParam.get("port")!!.toInt())
    if(!redisParam.get("password").equals("")){
        jedis.auth(redisParam.get("password"))
    }
    jedis.select(redisParam.get("db")!!.toInt())
    var result =""
    val webDriver =id.corechain.WebDriver().getWebDriver()
    if(webDriver!=null){
        webDriver.get("https://m2u.maybank.co.id/common/Login.do")
        webDriver.switchTo().defaultContent()
        var frame = webDriver.findElement(By.xpath("//frame"))
        webDriver.switchTo().frame(frame)
        webDriver.findElement(By.xpath("//button")).click()
        var element = webDriver.findElement(By.name("userName"))
        element.sendKeys(username)
        webDriver.findElement(By.xpath("//input[@name='action']")).click()
        element = webDriver.findElement(By.name("password"))
        element.sendKeys(password)

        webDriver.findElement(By.xpath("//input[@name='action']")).click()
        webDriver.findElement(By.linkText("Rekening dan Transaksi")).click()
        jedis.set("Maybank:lastLogin",Date().time.toString())
        var table_element = webDriver.findElement(By.xpath("//table[@class='tabsummary']//tbody//tr//td[@class='balance']"))
        var saldo =table_element.text
        var now =System.currentTimeMillis() / 1000L
        saldo = saldo.replace(".",":")
        saldo = saldo.replace(",",".")
        saldo = saldo.replace(":",",")
        result=result+"{\"bank\":\"MayBank\",\"amount\":\""+saldo+"\",\"lastupdate\":\""+now+"\"}"
        jedis.set(username+":MayBank:"+now,result)
        jedis.set(username+":MayBank:lastest",result)

        result ="result :["
        webDriver.findElement(By.xpath("//table[@class='tabsummary']//tbody//tr//td[@class='title']//a")).click()
        webDriver.findElement(By.linkText("Transaksi Hari Ini")).click()

        var tr_collection:List<WebElement>
        try {
            tr_collection = webDriver.findElements(By.xpath("//table[@class='tabdata']//tbody//tr"))
        }catch (e:Exception){
            result =result+"]"
            return  result
        }

        var row_num: Int
        var col_num: Int
        row_num = 1
        var first =true;
        var counter=0
        for (trElement in tr_collection) {
            val td_collection = trElement.findElements(By.xpath("td"))
            if(!first){
                result=result+","
            }
            first=false
            col_num = 1
            var res=""
            var date=""
            var desc=""
            var type=""
            var amount=""
            var indicator:String=""
            for (tdElement in td_collection) {
                if(td_collection.size==1){
                    continue
                }
                if(col_num==1){
                    res=res+"{\"Tanggal\":\""+tdElement.text+"\","
                    date=tdElement.text
                }else if(col_num==2){
                    res=res+"\"Keterangan Transaksi\":\""+tdElement.text+"\","
                    desc=tdElement.text
                }else if(col_num==3){
                    res=res+"\"Debet\":\""+tdElement.text+"\","
                    type="out"
                    amount=tdElement.text
                }else if(col_num==4){
                    res=res+"\"Kredit\":\""+tdElement.text+"\","
                    if(amount.equals("")){
                        type="in"
                        amount=tdElement.text
                    }
                }else if(col_num==5){
                    res=res+"\"Saldo\":\""+tdElement.text+"\"}"
                }
                col_num++
            }

            if(td_collection.size>1){
                var dates =date.split("/")
                var amounts = amount.split(".")
                amount=amounts[0]
                amount = amount.replace(",",".")
                amount = amount.replace("IDR ","")

                date=dates[2]+dates[1]+dates[0]

                val r: RethinkDB = RethinkDB.r;
                val conn: Connection = r.connection().hostname("rth0.corechain.id").port(28015).db(id.corechain.WebDriver().getDBName()).user("digiro","digiro").connect()
                r.table("mutasi").insert(r.hashMap("id", sha1converter(res)).with("date",date.toInt()).with("desc",desc).with("type",type).with("amount",amount).with("processed",-1).with("bank","Maybank")
                ).run<String>(conn)
                conn.close()
            }
            jedis.set(username+":MayBank:"+ sha1converter(res),res)
            result=result+res;
            row_num++
            counter++
        }
        result =result+"]"
        webDriver.findElement(By.linkText("Keluar")).click()
        jedis.quit()
        webDriver.quit()
    }
    return result;
}

fun sha1converter(param: String): String {
    return DigestUtils.sha1Hex(param)
}